import torch
from torch import nn
import pennylane as qml
from pennylane import numpy as np


n_qubits = 8
q_delta = 0.01              # Initial spread of random quantum weights
q_depth = 6                 # Depth of the quantum circuit (number of variational layers)

dev = qml.device("default.qubit", wires=n_qubits)

@qml.qnode(dev, interface="torch")
def quantum_net(q_input_features, q_weights_flat):
    q_weights = q_weights_flat.reshape(q_depth, n_qubits)
    for idx in range(n_qubits):
        qml.Hadamard(wires=idx)
    for idx, element in enumerate(q_input_features):
        qml.RY(element, wires=idx)
    for k in range(q_depth):
        for i in range(0, n_qubits - 1, 2):
            qml.CNOT(wires=[i, i + 1])
        for i in range(1, n_qubits - 1, 2):
            qml.CNOT(wires=[i, i + 1])
        for idx, element in enumerate(q_weights[k]):
            qml.RY(element, wires=idx)
    exp_vals = [qml.expval(qml.PauliZ(position)) for position in range(n_qubits)]
    return tuple(exp_vals)


class quantum_part_2_layers(nn.Module):
    def __init__(self, n_features):
        super(quantum_part_2_layers, self).__init__()
        self.pre_net = nn.Linear(n_features, n_qubits)
        self.q_params_1 = nn.Parameter(q_delta * torch.randn(q_depth * n_qubits))
        self.q_params_2 = nn.Parameter(q_delta * torch.randn(q_depth * n_qubits))
        self.post_net = nn.Linear(n_qubits, 1)

    def forward(self, input_features):
        pre_out = self.pre_net(input_features)
        q_in_1 = torch.tanh(pre_out) * np.pi / 2.0
        q_out_1 = torch.Tensor(0, n_qubits)
        for elem in q_in_1:
            q_out_elem_1 = quantum_net(elem, self.q_params_1).float().unsqueeze(0)
            q_out_1 = torch.cat((q_out_1, q_out_elem_1))
        q_in_2 = torch.tanh(q_out_1) * np.pi / 2.0
        q_out_2 = torch.Tensor(0, n_qubits)
        for elem in q_in_2:
            q_out_elem_2 = quantum_net(elem, self.q_params_2).float().unsqueeze(0)
            q_out_2 = torch.cat((q_out_2, q_out_elem_2))
        return self.post_net(q_out_2)