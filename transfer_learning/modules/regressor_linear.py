import torch
from torch import nn
from torch.nn import functional as F


class regressor_linear(nn.Module):
    def __init__(self):
        super(regressor_linear, self).__init__()

        self.efc0 = nn.Linear(1100, 500)
        self.efc0_bn = nn.BatchNorm1d(500)
        self.efc1 = nn.Linear(500, 100)
        self.efc1_bn = nn.BatchNorm1d(100)
        self.efc2 = nn.Linear(100, 25)
        self.efc2_bn = nn.BatchNorm1d(25)
        self.efc3 = nn.Linear(25, 1)

    def forward(self, x):
        h0 = F.relu(self.efc0_bn(self.efc0(x.view(-1, 1100))))
        h1 = F.relu(self.efc1_bn(self.efc1(h0)))
        h2 = F.relu(self.efc2_bn(self.efc2(h1)))
        return self.efc3(h2)
