import torch
from torch import nn
import pennylane as qml
from pennylane import numpy as np


n_qubits = 5
q_delta = 0.01              # Initial spread of random quantum weights
q_depth = 1                 # Depth of the quantum circuit (number of variational layers)

dev = qml.device("default.qubit", wires=n_qubits)

@qml.qnode(dev, interface="torch")
def quantum_net(q_input_features, q_weights_flat):
    q_weights = q_weights_flat.reshape(q_depth, n_qubits)
    for idx in range(n_qubits):
        qml.Hadamard(wires=idx)
    for idx, element in enumerate(q_input_features):
        qml.RY(element, wires=idx)
    for k in range(q_depth):
        for i in range(0, n_qubits - 1, 2):
            qml.CNOT(wires=[i, i + 1])
        for i in range(1, n_qubits - 1, 2):
            qml.CNOT(wires=[i, i + 1])
        for idx, element in enumerate(q_weights[k]):
            qml.RY(element, wires=idx)
    exp_vals = [qml.expval(qml.PauliZ(position)) for position in range(n_qubits)]
    return tuple(exp_vals)


class quantum_part(nn.Module):
    def __init__(self, n_features):
        super(quantum_part, self).__init__()
        self.pre_net = nn.Linear(n_features, n_qubits)
        self.q_params = nn.Parameter(q_delta * torch.randn(q_depth * n_qubits))
        self.post_net = nn.Linear(n_qubits, 1)

    def forward(self, input_features):
        pre_out = self.pre_net(input_features)
        q_in = torch.relu(pre_out) * np.pi / 2.0
        q_out = torch.Tensor(0, n_qubits)
        for elem in q_in:
            q_out_elem = quantum_net(elem, self.q_params).float().unsqueeze(0)
            print(q_out_elem)
            q_out = torch.cat((q_out, q_out_elem))
        return self.post_net(q_out)