import torch
from torch import nn
import numpy as np
from qiskit import QuantumCircuit, IBMQ, execute

IBMQ.load_account()
provider = IBMQ.get_provider('ibm-q')
backend = provider.get_backend('ibmq_quito')

n_qubits = 5
q_delta = 0.01              # Initial spread of random quantum weights
q_depth = 1                 # Depth of the quantum circuit (number of variational layers)


def quantum_net(q_input_features, q_weights_flat):
    q_weights = q_weights_flat.reshape(q_depth, n_qubits)
    qc = QuantumCircuit(n_qubits, n_qubits)
    for idx in range(n_qubits):
        qc.h(idx)
    for idx, element in enumerate(q_input_features):
        qc.ry(element.item(), idx)
    for k in range(q_depth):
        for i in range(0, n_qubits - 1, 2):
            qc.cnot(i, i+1)
        for i in range(1, n_qubits - 1, 2):
            qc.cnot(i, i+1)
        for idx, element in enumerate(q_weights[k]):
            qc.ry(element.item(), idx)
    for idx in range(n_qubits):
        qc.measure(idx, idx)
    backend = Aer.get_backend('aer_simulator')
    result = execute(qc, backend, shots=1024).result()
    counts = result.get_counts()
    exp_vals = []
    for idx in range(n_qubits):
        val = 0
        for key in counts.keys():
            if key[len(key)-1-idx] == '0':
                val += (counts[key]/1024.)**2
            else:
                val -= (counts[key]/1024.)**2
        exp_vals.append(val)
    return torch.tensor([exp_vals])


class quantum_part_ibm_simulator(nn.Module):
    def __init__(self, n_features):
        super(quantum_part_ibm_simulator, self).__init__()
        self.pre_net = nn.Linear(n_features, n_qubits)
        self.q_params = nn.Parameter(q_delta * torch.randn(q_depth * n_qubits))
        self.post_net = nn.Linear(n_qubits, 1)

    def forward(self, input_features):
        pre_out = self.pre_net(input_features)
        q_in = torch.relu(pre_out) * np.pi / 2.0
        q_out = torch.Tensor(0, n_qubits)
        for elem in q_in:
            q_out_elem = quantum_net(elem, self.q_params)
            q_out = torch.cat((q_out, q_out_elem))
        return self.post_net(q_out)