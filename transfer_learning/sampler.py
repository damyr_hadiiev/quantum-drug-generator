import os
import torch
from sklearn.metrics import r2_score
from modules.smiles import *
import matplotlib.pyplot as plt


kwargs = {}

path_to_model = 'Results/Classical Transfer 7/models/model_best_test.pt'
#path_to_model = 'models/model_best_train.pt'
#path_to_model = 'models/model.pt'
path_to_dictionary = 'data/Dictionary'

if os.path.isfile(path_to_model):
    fh = open('exp_vs_pred.tsv', 'w')
    model = torch.load(path_to_model)

    smiles_dict = make_smiles_dict(path_to_dictionary)
    size_of_smiles_dict = len(smiles_dict)

    #sample_dataset = SMILESDataset(path_to_csv_file='data/smiles.train.csv', size_of_smiles_dict=size_of_smiles_dict)
    sample_dataset = SMILESDataset(path_to_csv_file='data/smiles.test.csv', size_of_smiles_dict=size_of_smiles_dict)
    number_of_columns = sample_dataset.col_num()
    sample_loader = torch.utils.data.DataLoader(sample_dataset, batch_size=1, shuffle=False, **kwargs)

    model.eval()
    y_e = []
    y_p = []
    for (y, x) in sample_loader:
        with torch.no_grad():
            recon_y = model(x)
        for (y0, y1) in zip(y, recon_y):
            y0_f = round(float(y0[0]), 2)
            y1_f = round(float(y1[0]), 2)
            fh.write(str(y0_f) + '\t' + str(y1_f) + '\n')
            y_e.append(y0_f)
            y_p.append(y1_f)
    fh.close()
    print(r2_score(y_e, y_p))
    plt.scatter(y_p, y_e)
    plt.show()
else:
    print('Error: Model not found, check the path ', path_to_model)
