# Enhancing ReleaSE Algorithm with Quantum Computing

## Introduction
In this project, we use quantum computing to improve on a computational strategy for de novo drug discovery for molecules with desired properties called Reinforcement Learning for Structural Evolution (ReLeaSE) (see reference below) using Quantum Machine Learning (QML). The main parts of the ReLeaSE workflow are two neural networks, one generative and one predictive. By changing the classical neural network to quantum neural networks, we aim to show that firstly, QML techniques may offer a training advantage to their classical counterpart, and secondly, they are viable in the NISQ-era.

Popova, M., Isayev, O., & Tropsha, A. (2018). Deep reinforcement learning for de novo drug design. Science advances, 4(7), eaap7885.

## Generator + Entire Reinforcement Learning Workflow

For the generator, we constructed a quantum version of GRU, a Recurrent Neural Network techniques. To look at the workings of the quantum generator as well as to see how the entire reinforcement learning process works, stay in the main branch, and go to the folder:

ReleaSE_multiobjective

A more detailed explanation would be given there.

## Predictor

For the predictors, we made predictors for logP values for SMILES using two quantum methods: Quantum Transfer Learning, and Quanvolutional Neural Networks. Quantum Tranfer Learning is a quantum counterpart of the ML technique Transfer Learning, while Quanvolutional Neural Networks is the quanutm counterpart to convolutional neural networks. For the former (Quantum Transfer Learning), please go to the branch:

transfer_learning

and explore the code there.

For the latter (Quanvolutional Neural Networks), please go to the branch:

quanvolutional_smiles_logp

and explore the notebooks there.

## DGX

In the process of doing this project, we also attempted to speed things up using the Nvidia CuQuantum DQX simulator, and we did some benchmarks on pennylane. This can be found in the main branch, in the folder named pennylane_benchmark.

## Authors and acknowledgment
This project was done collaboratively with Jonas Tan (Quantum GRU), Anastasiia Kudryk (Quantum Transfer Learning), Nataliia Susulovska (Quanvolutional Neural Networks) and Mykhailo Ohorodnikov (Benchmarking) under the supervision of Max Druchok and Damyr Hadiiev.

## Project status
This project is currently on-hold, with all the POCs out. More experimentation is needed, and we may come back to it at a later date.
