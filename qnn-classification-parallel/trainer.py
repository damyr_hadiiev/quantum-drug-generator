import os
import time
import numpy as np

import torch
import torch.distributed as dist
import torch.nn as nn
import torch.optim as optim
from torch.nn.parallel import DistributedDataParallel as DDP

import torchvision.datasets
import torchvision.transforms

from ClassificationModel import *

def create_train_loader_MNIST(batch_size, n_train=None):

    transform = torchvision.transforms.Compose(
        [torchvision.transforms.ToTensor(),
         torchvision.transforms.Normalize((0.1307,), (0.3081,))])

    train_dataset = torchvision.datasets.MNIST(root='./data', train=True, download=True, transform=transform)

    if n_train != None:
        train_dataset = torch.utils.data.random_split(train_dataset, [n_train, len(train_dataset) - n_train],
                                                      generator=torch.Generator().manual_seed(42))[0]

    train_sampler = torch.utils.data.distributed.DistributedSampler(dataset=train_dataset, shuffle=True)
    # arguments rank and world_size are retrieved from the current distributed group by default

    train_loader = torch.utils.data.DataLoader(dataset=train_dataset, batch_size=batch_size, sampler=train_sampler,
                                               pin_memory=True)

    return train_loader


def create_test_loader_MNIST(batch_size, n_test=None):

    transform = torchvision.transforms.Compose(
        [torchvision.transforms.ToTensor(),
         torchvision.transforms.Normalize((0.1307,), (0.3081,))])

    test_dataset = torchvision.datasets.MNIST(root='./data', train=False, download=True, transform=transform)
    if n_test != None:
        test_dataset = torch.utils.data.random_split(test_dataset, [n_test, len(test_dataset) - n_test],
                                                      generator=torch.Generator().manual_seed(42))[0]
    test_sampler = torch.utils.data.distributed.DistributedSampler(dataset=test_dataset, shuffle=True)
    test_loader = torch.utils.data.DataLoader(dataset=test_dataset, batch_size=batch_size, sampler=test_sampler,
                                              pin_memory=True)

    return test_loader


def setup(rank, world_size):

    os.environ['MASTER_ADDR'] = 'localhost'
    os.environ['MASTER_PORT'] = '12355'
    
    # initialize the process group
    dist.init_process_group(backend="nccl", init_method="env://", rank=rank, world_size=world_size)
    # dist.init_process_group("gloo", rank=rank, world_size=world_size)

def cleanup():

    dist.destroy_process_group()


def train(rank, args):

    # initialize the process group
    setup(rank, args.world_size)

    # set the device with rank id
    torch.cuda.device(rank)

    # use seed to make sure that the models initialized in different processes are the same
    torch.manual_seed(0)

    # initialize the model
    if args.model_type == 'CNN':
        model = CNN(num_classes=10)
        file_name = 'cnn_model.pt'
    elif args.model_type == 'CNNHenderson':
        model = CNNHenderson(num_classes=10)
        file_name = 'cnn_henderson_model.pt'
    elif args.model_type == 'QNN':
        model = QNN(rank, num_classes=10, num_filters=args.num_filters)
        file_name = 'qnn_model.pt'
    elif args.model_type == 'QNNHenderson':
        model = QNNHenderson(rank, num_classes=10, num_filters=args.num_filters)
        file_name = 'qnn_henderson_model.pt'
    else:
        raise Exception('Only CNN, CNNHenderson, QNN and QNNHenderson models are currently available.')

    # path to save the model after training
    path_to_model = args.model_prefix + file_name

    # move model to the GPU with rank id
    model = model.to(rank)
    # use the next line if the model contains any BatchNorm*D layers
    model = nn.SyncBatchNorm.convert_sync_batchnorm(model)

    # wrap the model with DDP
    ddp_model = DDP(model, device_ids=[rank])
    
    # initialize loss function and optimizer
    loss_function = nn.CrossEntropyLoss()
    optimizer = optim.SGD(ddp_model.parameters(), lr=args.learning_rate)
    # move loss function to the GPU with rank id (if the loss function has some intrinsic parameters)
    loss_function = loss_function.to(rank)

    # create a train_loader
    train_loader = create_train_loader_MNIST(args.batch_size, args.train_dataset_size)

    num_samples = len(train_loader.dataset)
    num_batches = int(np.ceil(num_samples / args.batch_size))
    num_batches_per_gpu = len(train_loader)

    print('Number of training samples:' + str(num_samples))
    print('Number of batches: ' + str(num_batches))

    start = time.time()

    for epoch in range(1, args.num_epochs + 1):

        train_loader.sampler.set_epoch(epoch)

        ddp_model.train()

        for batch_idx, (image, label) in enumerate(train_loader):

            # print('Rank: {}  Epoch: {}  Batch index: {}'.format(rank, epoch, batch_idx))
            # print(label.size())

            # move input data to the GPU with id rank
            image = image.to(rank)
            label = label.to(rank)

            # set the parameter gradients to zero
            optimizer.zero_grad()

            # forward + backward + optimize
            recon_label = ddp_model(image)
            loss = loss_function(recon_label, label)
            loss.backward()
            optimizer.step()

            if (batch_idx + 1) % args.log_interval == 0 and rank == 0:
                print('Epoch [{}/{}], Step [{}/{}], Loss: {:.4f}'.format(
                    epoch,
                    args.num_epochs,
                    batch_idx + 1,
                    num_batches_per_gpu,
                    loss.item())
                )

    end = time.time()

    if rank == 0:
        print("Training complete in: " + str(end - start))
        torch.save(ddp_model.module.state_dict(), path_to_model)

    cleanup()