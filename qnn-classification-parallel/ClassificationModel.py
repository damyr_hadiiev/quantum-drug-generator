import torch
import torch.nn as nn

from quanvolution_gpu import *

class CNN(nn.Module):
    def __init__(self, num_classes=10):
        super(CNN, self).__init__()
        self.conv1 = nn.Sequential(
            nn.Conv2d(1, 16, kernel_size=5, stride=1, padding=2),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2))
        self.conv2 = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=5, stride=1, padding=2),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2))
        self.fc = nn.Linear(7*7*32, num_classes)

    def forward(self, x):
        out = self.conv1(x)
        out = self.conv2(out)
        out = out.reshape(out.size(0), -1)
        out = self.fc(out)
        return out


class CNNHenderson(nn.Module):
    def __init__(self, num_classes=10):
        super(CNNHenderson, self).__init__()

        self.conv1 = nn.Sequential(
            # Tensor - (N_batch, Channels = 1, Height = 28, Width = 28)
            nn.Conv2d(1, 50, kernel_size=5, stride=1),
            # Tensor - (N_batch, Channels = 50, Height = 24, Width = 24)
            nn.BatchNorm2d(50),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )

        self.conv2 = nn.Sequential(
            # Tensor - (N_batch, Channels = 50, Height = 12, Width = 12)
            nn.Conv2d(50, 64, kernel_size=5, stride=1),
            # Tensor - (N_batch, Channels = 64, Height = 8, Width = 8)
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2)
            # Tensor - (N_batch, Channels = 64, Height = 4, Width = 4)
        )

        self.fc1 = nn.Sequential(
            nn.Linear(4 * 4 * 64, num_classes),
            nn.BatchNorm1d(num_classes),
            nn.ReLU(),
            nn.Dropout(p=0.4)
        )

        self.fc2 = nn.Linear(num_classes, num_classes)

    def forward(self, x):
        out = self.conv1(x)
        out = self.conv2(out)
        out = out.reshape(out.size(0), -1)
        out = self.fc1(out)
        out = self.fc2(out)
        return out

class QNN(nn.Module):
    def __init__(self, rank, num_classes=10, num_filters=1):
        super(QNN, self).__init__()

        self.quanv = nn.Sequential(
            Quanvolution(rank=rank, out_channels=num_filters, kernel_size=(2,2), stride=(2,2)),
            nn.BatchNorm2d(num_filters),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )

        self.fc = nn.Linear(7*7*num_filters, num_classes)

    def forward(self, x):
        out = self.quanv(x)
        out = out.reshape(out.size(0), -1)
        out = self.fc(out)
        return out

class QNNHenderson(nn.Module):
    def __init__(self, rank, num_classes=10, num_filters=1):
        super(QNNHenderson, self).__init__()

        self.quanv = nn.Sequential(
            # Tensor - (N_batch, Channels = 1, Height = 28, Width = 28)
            Quanvolution(rank=rank, out_channels=num_filters, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
            nn.BatchNorm2d(num_filters),
            nn.ReLU()
        )

        self.conv1 = nn.Sequential(
            # Tensor - (N_batch, Channels = 1, Height = 28, Width = 28)
            nn.Conv2d(1, 50, kernel_size=5, stride=1),
            # Tensor - (N_batch, Channels = 50, Height = 24, Width = 24)
            nn.BatchNorm2d(50),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )

        self.conv2 = nn.Sequential(
            # Tensor - (N_batch, Channels = 50, Height = 12, Width = 12)
            nn.Conv2d(50, 64, kernel_size=5, stride=1),
            # Tensor - (N_batch, Channels = 64, Height = 8, Width = 8)
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2)
            # Tensor - (N_batch, Channels = 64, Height = 4, Width = 4)
        )

        self.fc1 = nn.Sequential(
            nn.Linear(4 * 4 * 64, num_classes),
            nn.BatchNorm1d(num_classes),
            nn.ReLU(),
            nn.Dropout(p=0.4)
        )

        self.fc2 = nn.Linear(num_classes, num_classes)

    def forward(self, x):
        out = self.quanv(x)
        out = self.conv1(out)
        out = self.conv2(out)
        out = out.reshape(out.size(0), -1)
        out = self.fc1(out)
        out = self.fc2(out)
        return out
