{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "4679b0a8",
   "metadata": {},
   "source": [
    "# Quanvolutional Neural Network (QNN)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5dac337a",
   "metadata": {},
   "source": [
    "<p style='text-align: justify;'>  Henderson et al (2020) introduced the concept of a quantum convolutional neural network (quanvolutional neural network or QNN) as an extension of the classical CNN aiming to leverage the advantages of quantum computing techniques while taking into account the limitations of the available quantum hardware. Architecturally, QNNs are very similar to their classical counterparts. However, a new type of transformational layer – a quanvolutional layer – is introduced. It consists of a set of filters (kernels) each represented by a parametrized quantum circuit. Even though a quanvolutional layer differs from its classical predecessor in the nature of transformations to the data, the fundamental idea behind it remains. Namely, the input data is locally processed by a sequence of filters to produce abstract feature maps. Thus, a quanvolutional layer can be viewed as a quantum subroutine in an otherwise classical machine learning model. This means that conceptually the QNN framework follows a hybrid quantum-classical approach and could potentially bring some degree of improvement compared to the classical models when implemented on NISQ era quantum devices. It was experimentally shown that in comparison to classical CNNs it exhibits higher test set accuracy as well as shorter training time when applied to the image classification problem.</p>\n",
    "\n",
    "<p style='text-align: justify;'> QNN framework also has a range of advantages compared to many other QML models. Firstly, due to the local character of the quanvolutional transformations the requirement on the number of qubits is relatively low. Moreover, parametrized circuits playing the role of quanvolutional filters are relatively shallow. Another powerful benefit of the quanvolutional algorithm is its potential resiliency to the consistent noise models.</p>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c3089630",
   "metadata": {},
   "source": [
    "![SNOWFALL](QNN_fig_1.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "55f0444c",
   "metadata": {},
   "outputs": [],
   "source": [
    "import torch\n",
    "from torch import nn\n",
    "from torch.nn import functional as F\n",
    "import pennylane as qml\n",
    "import numpy as np\n",
    "from numpy.random import default_rng\n",
    "from pennylane.templates import AngleEmbedding, RandomLayers\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "88a02815",
   "metadata": {},
   "source": [
    "## Quanvolutional Layer"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8249ecd0",
   "metadata": {},
   "source": [
    "<p style='text-align: justify;'> Quanvolutional layer can be flexibly integrated into any classical neural network stack as shown in the figure. The number of layers, their place in the stack, the number of filters in each layer and the layer configuration are left to the user’s specification without any necessity of revisiting the internal workings of the quantum filters. In general, successful integration of any quantum subroutine into a classical machine learning model depends on developing efficient methods of encoding classical data into a quantum state as well as interpreting the results of quantum computations. </p>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "68b6474d",
   "metadata": {},
   "source": [
    "### Parametrized Quantum Circuit as a Quanvolutional Filter"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8de53e96",
   "metadata": {},
   "source": [
    "The application of a quanvolutional filter is carried out in three main stages.\n",
    "\n",
    "1. **Encoding**\n",
    "\n",
    "      A subsection of an input tensor is encoded into a quantum state by a certain consistent encoding method. The most popular technique is threshold encoding, which maps each element of the input to a separate qubit initialized in state 0, if the value of this element does not exceed some predetermined threshold value, and to state 1 otherwise. Another widely used method, which is also exploited in our implementation, embeds classical data into a quantum circuit with the help of rotational gates (e.g., RY gates), where the input element value is passed as the rotational parameter to the gate acting on the corresponding qubit. Note that the number of qubits in the circuit is determined by the size of the kernel and is equal to its height times its width. The size of the kernel is usually chosen from intuitive considerations of the specific spatial structure of the data at hand. There are also some encoding methods, which are more efficient in terms of the number of required qubits, including FQIR (flexible quantum image representation). \n",
    "\n",
    "\n",
    "2. **Quanvolution**\n",
    "\n",
    "    A unitary transformation represented by a parametrized quantum circuit is applied to the initialized quantum state. This circuit can be either random or structured. In our implementation the quanvolutional circuit is composed of random layers containing single-qubit parametrized rotational gates (RX, RY, RZ) and two-qubit entangling gates (CX) acting on randomly chosen qubits. A user is provided with an option to variate the depth of the quantum circuit by specifying the number of random layers when setting up a QNN.\n",
    "    \n",
    "    \n",
    "3. **Decoding**\n",
    "\n",
    "    As a final step, a series of measurements are performed on the qubit system in order to decode the resulting quantum state. We measure each qubit in the computational basis and obtain a list of Pauli-Z expectation values. These scalar values can be either directly mapped to a separate output channel each (in which case the number of output feature maps from a single filter is equal to the number of qubits) or postprocessed with the help of classical methods. We follow the second approach and output an average of all the obtained expectation values, which takes its respective place in a single feature map.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "id": "6f9cec51",
   "metadata": {},
   "outputs": [],
   "source": [
    "def quanvolution_circuit(q_input, rand_params, seed=42):\n",
    "    \"\"\"A quantum circuit function corresponding to the quanvolutional kernel.\"\"\"\n",
    "    n_qubits = len(q_input)\n",
    "\n",
    "    # Encode (n_qubits) classical input values into a quantum state by the action of RY gates\n",
    "    qml.AngleEmbedding(np.pi * q_input, wires=range(n_qubits), rotation='Y')\n",
    "\n",
    "    # Apply a random unitary transformation (a combination of parametrized rotational gates RX, RY, RZ\n",
    "    # and entangling gates CX)\n",
    "    qml.RandomLayers(rand_params, wires=list(range(n_qubits)), seed=seed)\n",
    "\n",
    "    # Measure each qubit in the computational basis to produce (n_qubits) classical output values\n",
    "    meas_res = [qml.expval(qml.PauliZ(qubit)) for qubit in range(n_qubits)]\n",
    "    return tuple(meas_res)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "id": "b7e143ef",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0: ──RY(12.57)──RX(0.81)─╭X──RY(0.61)──RX(0.73)─┤  <Z>\n",
      "1: ──RY(5.33)──╭X────────│──────────────────────┤  <Z>\n",
      "2: ──RY(0.81)──╰●────────│──────────────────────┤  <Z>\n",
      "3: ──RY(0.33)───RZ(0.91)─╰●─────────────────────┤  <Z>\n"
     ]
    }
   ],
   "source": [
    "# Usage example\n",
    "\n",
    "# Set up a quantum circuit\n",
    "dev = qml.device('default.qubit', wires=4)\n",
    "test_circuit = qml.QNode(quanvolution_circuit, dev)\n",
    "n_quanv_layers = 1\n",
    "\n",
    "# Generate test input data and parameters for the random circuit\n",
    "rng = default_rng(0)\n",
    "test_q_input = rng.uniform(0, 2*np.pi, size=4)\n",
    "test_rand_params = rng.random(size=(n_quanv_layers, len(test_q_input)))    \n",
    "\n",
    "# Visualize the quantum circuit\n",
    "print(qml.draw(test_circuit, expansion_strategy=\"device\")(test_q_input, test_rand_params))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "97722703",
   "metadata": {},
   "source": [
    "<p style='text-align: justify;'> This procedure is repeated iteratively by moving the kernel over the input tensor as a sliding window with a given stride (the distance between two consecutive positions of the kernel over the input). As a result, a feature map is obtained. It’s a general practice to apply a number of filters in parallel in order to obtain multiple feature maps holding information about different data patterns. </p>\n",
    "\n",
    "<p style='text-align: justify;'> Quanvolutional circuits can be either variational (trainable via backpropagation) or have a fixed set of parameters (untrainable). Note that in either case the same circuit is applied to multiple subsections of the input tensor. Therefore, in the trainable mode instead of learning a separate set of parameters corresponding to each input subsection, only one set has to be learnt. This helps to effectively cut down on the number of the model parameters. For simplicity we stick with untrainable circuits in our QNN implementation.</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "id": "8481b9b2",
   "metadata": {},
   "outputs": [],
   "source": [
    "class Quanvolution(nn.Module):\n",
    "    \"\"\"A quantum extension of a convolutional layer with classical filters replaced by parametrized\n",
    "       quantum circuits. Consists of (out_channels) filters that locally transform input data in a\n",
    "       sliding window manner to produce feature maps.\n",
    "        Args:\n",
    "            out_channels (int): Number of channels produced by the convolution (number of filters)\n",
    "            kernel_size (tuple): Size of the convolving kernel.\n",
    "            stride (tuple): Stride of the convolution (distance between two consecutive\n",
    "                            positions of the kernel over the input tensor).\n",
    "            padding (tuple): Amount of zero-padding added to the sides of the input. Default: (0,0)\n",
    "            n_quanv_layers (int): Number of random layers in the quantum circuit corresponding to the\n",
    "                                  convolving kernel. Default: 1\n",
    "    \"\"\"\n",
    "    def __init__(self, out_channels=None, kernel_size=None, stride=None, padding=(0, 0), n_quanv_layers=1):\n",
    "        super(Quanvolution, self).__init__()\n",
    "        self.out_channels = out_channels\n",
    "        self.kernel_size = kernel_size\n",
    "        self.stride = stride\n",
    "        self.padding = padding\n",
    "        self.n_quanv_layers = n_quanv_layers\n",
    "        self.n_qubits = int(np.prod(np.array(self.kernel_size)))\n",
    "        self.dev = qml.device('default.qubit', wires=self.n_qubits)\n",
    "        self.circuit = qml.QNode(quanvolution_circuit, self.dev, interface=\"torch\")\n",
    "\n",
    "    def __str__(self):\n",
    "        return \"Quanvolution(out_channels=%s, kernel_size=%s, stride=%s, padding=%s, n_quanv_layers=%s)\" \\\n",
    "               % (self.out_channels, self.kernel_size, self.stride, self.padding, self.n_quanv_layers)\n",
    "\n",
    "    def __repr__(self):\n",
    "        return \"Quanvolution(out_channels=%s, kernel_size=%s, stride=%s, padding=%s, n_quanv_layers=%s)\" \\\n",
    "               % (self.out_channels, self.kernel_size, self.stride, self.padding, self.n_quanv_layers)\n",
    "\n",
    "    def forward(self, input_data):\n",
    "        batch_size = input_data.size(dim=0)\n",
    "        in_channels = input_data.size(dim=1)\n",
    "        height = input_data.size(dim=2)\n",
    "        width = input_data.size(dim=3)\n",
    "\n",
    "        # Determine the size of the output tensor\n",
    "        out_size = ((height + 2 * self.padding[0] - self.kernel_size[0]) // self.stride[0] + 1,\n",
    "                    (width + 2 * self.padding[1] - self.kernel_size[1]) // self.stride[1] + 1)\n",
    "\n",
    "        # Apply zero-padding of the given size to the input data\n",
    "        vertical_padding = torch.zeros(batch_size, in_channels, self.padding[0], width)\n",
    "        horizontal_padding = torch.zeros(batch_size, in_channels, height + 2 * self.padding[0], self.padding[1])\n",
    "        input_data = torch.cat((vertical_padding, input_data, vertical_padding), dim=2)\n",
    "        input_data = torch.cat((horizontal_padding, input_data, horizontal_padding), dim=3)\n",
    "\n",
    "        # Set up and seed random number generators\n",
    "        torch.manual_seed(0)\n",
    "        rng = default_rng(0)\n",
    "\n",
    "        # Create an empty tensor to store output feature maps\n",
    "        output_features = torch.zeros(batch_size, self.out_channels, in_channels, out_size[0], out_size[1])\n",
    "\n",
    "        # Quanvolution\n",
    "        for f in range(self.out_channels):\n",
    "            for c in range(in_channels):\n",
    "                # Generate random parameters for a random circuit representing a quanvolutional kernel\n",
    "                rand_params = torch.nn.Parameter(2 * np.pi * torch.rand(self.n_quanv_layers, self.n_qubits), requires_grad=False)\n",
    "                # Generate a random seed for the random circuit\n",
    "                circuit_seed = rng.integers(1000, size=1)\n",
    "\n",
    "                for smiles in range(batch_size):\n",
    "                    print('Filter: ' + str(f+1) + '  ' + 'Channel: ' + str(c+1) + '  ' + 'SMILE:' + str(smiles+1))\n",
    "                    conv_row = -1\n",
    "\n",
    "                    for i in range(0, height, self.stride[0]):\n",
    "                        if i + self.kernel_size[0] > height:\n",
    "                            break\n",
    "                        else:\n",
    "                            conv_column = -1\n",
    "                            conv_row += 1\n",
    "\n",
    "                            for j in range(0, width, self.stride[1]):\n",
    "                                if j + self.kernel_size[1] > width:\n",
    "                                    break\n",
    "                                else:\n",
    "                                    conv_column += 1\n",
    "                                    # Select the input subsection to be transformed\n",
    "                                    selected_input_area = input_data[smiles, c, i:i + self.kernel_size[0],\n",
    "                                                          j:j + self.kernel_size[1]]\n",
    "                                    q_in = torch.flatten(selected_input_area)\n",
    "                                    # Execute quantum circuit\n",
    "                                    q_out = self.circuit(q_in, rand_params, seed=circuit_seed)\n",
    "                                    # Postprocessing of quantum measurement results\n",
    "                                    average_out = (torch.sum(q_out) / self.n_qubits).item()\n",
    "                                    output_features[smiles][f][c][conv_row][conv_column] = average_out\n",
    "\n",
    "        # Summation over the input channels dimension\n",
    "        output_features = torch.sum(output_features, dim=2)\n",
    "\n",
    "        return output_features"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "26d08e71",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\"> <p style='text-align: justify;'> <font color=blue>Tip.</font> Be cautious when attempting to minimize the number of required qubits by decreasing the quanvolutional kernel size. Obviously, the smaller the kernel, the fewer qubits our circuit operates with. However, this comes at the expense of increasing the number of local transformations (the number of times the quantum circuit is executed), which might significantly effect the cumulative runtime of the algorithm. </p> </div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "279b6c7f",
   "metadata": {},
   "source": [
    "## Testing the Quanvolutional Layer Implementation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "id": "34374832",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Filter: 1  Channel: 1  SMILE:1\n",
      "Filter: 1  Channel: 1  SMILE:2\n",
      "Filter: 1  Channel: 1  SMILE:3\n",
      "Filter: 1  Channel: 1  SMILE:4\n",
      "Filter: 1  Channel: 1  SMILE:5\n",
      "Filter: 1  Channel: 1  SMILE:6\n",
      "Filter: 1  Channel: 1  SMILE:7\n",
      "Filter: 1  Channel: 1  SMILE:8\n",
      "Filter: 1  Channel: 1  SMILE:9\n",
      "Filter: 1  Channel: 1  SMILE:10\n",
      "Filter: 1  Channel: 2  SMILE:1\n",
      "Filter: 1  Channel: 2  SMILE:2\n",
      "Filter: 1  Channel: 2  SMILE:3\n",
      "Filter: 1  Channel: 2  SMILE:4\n",
      "Filter: 1  Channel: 2  SMILE:5\n",
      "Filter: 1  Channel: 2  SMILE:6\n",
      "Filter: 1  Channel: 2  SMILE:7\n",
      "Filter: 1  Channel: 2  SMILE:8\n",
      "Filter: 1  Channel: 2  SMILE:9\n",
      "Filter: 1  Channel: 2  SMILE:10\n",
      "Filter: 1  Channel: 3  SMILE:1\n",
      "Filter: 1  Channel: 3  SMILE:2\n",
      "Filter: 1  Channel: 3  SMILE:3\n",
      "Filter: 1  Channel: 3  SMILE:4\n",
      "Filter: 1  Channel: 3  SMILE:5\n",
      "Filter: 1  Channel: 3  SMILE:6\n",
      "Filter: 1  Channel: 3  SMILE:7\n",
      "Filter: 1  Channel: 3  SMILE:8\n",
      "Filter: 1  Channel: 3  SMILE:9\n",
      "Filter: 1  Channel: 3  SMILE:10\n",
      "Filter: 2  Channel: 1  SMILE:1\n",
      "Filter: 2  Channel: 1  SMILE:2\n",
      "Filter: 2  Channel: 1  SMILE:3\n",
      "Filter: 2  Channel: 1  SMILE:4\n",
      "Filter: 2  Channel: 1  SMILE:5\n",
      "Filter: 2  Channel: 1  SMILE:6\n",
      "Filter: 2  Channel: 1  SMILE:7\n",
      "Filter: 2  Channel: 1  SMILE:8\n",
      "Filter: 2  Channel: 1  SMILE:9\n",
      "Filter: 2  Channel: 1  SMILE:10\n",
      "Filter: 2  Channel: 2  SMILE:1\n",
      "Filter: 2  Channel: 2  SMILE:2\n",
      "Filter: 2  Channel: 2  SMILE:3\n",
      "Filter: 2  Channel: 2  SMILE:4\n",
      "Filter: 2  Channel: 2  SMILE:5\n",
      "Filter: 2  Channel: 2  SMILE:6\n",
      "Filter: 2  Channel: 2  SMILE:7\n",
      "Filter: 2  Channel: 2  SMILE:8\n",
      "Filter: 2  Channel: 2  SMILE:9\n",
      "Filter: 2  Channel: 2  SMILE:10\n",
      "Filter: 2  Channel: 3  SMILE:1\n",
      "Filter: 2  Channel: 3  SMILE:2\n",
      "Filter: 2  Channel: 3  SMILE:3\n",
      "Filter: 2  Channel: 3  SMILE:4\n",
      "Filter: 2  Channel: 3  SMILE:5\n",
      "Filter: 2  Channel: 3  SMILE:6\n",
      "Filter: 2  Channel: 3  SMILE:7\n",
      "Filter: 2  Channel: 3  SMILE:8\n",
      "Filter: 2  Channel: 3  SMILE:9\n",
      "Filter: 2  Channel: 3  SMILE:10\n",
      "Filter: 3  Channel: 1  SMILE:1\n",
      "Filter: 3  Channel: 1  SMILE:2\n",
      "Filter: 3  Channel: 1  SMILE:3\n",
      "Filter: 3  Channel: 1  SMILE:4\n",
      "Filter: 3  Channel: 1  SMILE:5\n",
      "Filter: 3  Channel: 1  SMILE:6\n",
      "Filter: 3  Channel: 1  SMILE:7\n",
      "Filter: 3  Channel: 1  SMILE:8\n",
      "Filter: 3  Channel: 1  SMILE:9\n",
      "Filter: 3  Channel: 1  SMILE:10\n",
      "Filter: 3  Channel: 2  SMILE:1\n",
      "Filter: 3  Channel: 2  SMILE:2\n",
      "Filter: 3  Channel: 2  SMILE:3\n",
      "Filter: 3  Channel: 2  SMILE:4\n",
      "Filter: 3  Channel: 2  SMILE:5\n",
      "Filter: 3  Channel: 2  SMILE:6\n",
      "Filter: 3  Channel: 2  SMILE:7\n",
      "Filter: 3  Channel: 2  SMILE:8\n",
      "Filter: 3  Channel: 2  SMILE:9\n",
      "Filter: 3  Channel: 2  SMILE:10\n",
      "Filter: 3  Channel: 3  SMILE:1\n",
      "Filter: 3  Channel: 3  SMILE:2\n",
      "Filter: 3  Channel: 3  SMILE:3\n",
      "Filter: 3  Channel: 3  SMILE:4\n",
      "Filter: 3  Channel: 3  SMILE:5\n",
      "Filter: 3  Channel: 3  SMILE:6\n",
      "Filter: 3  Channel: 3  SMILE:7\n",
      "Filter: 3  Channel: 3  SMILE:8\n",
      "Filter: 3  Channel: 3  SMILE:9\n",
      "Filter: 3  Channel: 3  SMILE:10\n",
      "Filter: 4  Channel: 1  SMILE:1\n",
      "Filter: 4  Channel: 1  SMILE:2\n",
      "Filter: 4  Channel: 1  SMILE:3\n",
      "Filter: 4  Channel: 1  SMILE:4\n",
      "Filter: 4  Channel: 1  SMILE:5\n",
      "Filter: 4  Channel: 1  SMILE:6\n",
      "Filter: 4  Channel: 1  SMILE:7\n",
      "Filter: 4  Channel: 1  SMILE:8\n",
      "Filter: 4  Channel: 1  SMILE:9\n",
      "Filter: 4  Channel: 1  SMILE:10\n",
      "Filter: 4  Channel: 2  SMILE:1\n",
      "Filter: 4  Channel: 2  SMILE:2\n",
      "Filter: 4  Channel: 2  SMILE:3\n",
      "Filter: 4  Channel: 2  SMILE:4\n",
      "Filter: 4  Channel: 2  SMILE:5\n",
      "Filter: 4  Channel: 2  SMILE:6\n",
      "Filter: 4  Channel: 2  SMILE:7\n",
      "Filter: 4  Channel: 2  SMILE:8\n",
      "Filter: 4  Channel: 2  SMILE:9\n",
      "Filter: 4  Channel: 2  SMILE:10\n",
      "Filter: 4  Channel: 3  SMILE:1\n",
      "Filter: 4  Channel: 3  SMILE:2\n",
      "Filter: 4  Channel: 3  SMILE:3\n",
      "Filter: 4  Channel: 3  SMILE:4\n",
      "Filter: 4  Channel: 3  SMILE:5\n",
      "Filter: 4  Channel: 3  SMILE:6\n",
      "Filter: 4  Channel: 3  SMILE:7\n",
      "Filter: 4  Channel: 3  SMILE:8\n",
      "Filter: 4  Channel: 3  SMILE:9\n",
      "Filter: 4  Channel: 3  SMILE:10\n"
     ]
    }
   ],
   "source": [
    "# Usage example\n",
    "\n",
    "# Generate a random input tensor\n",
    "test_input_tensor = torch.rand(size=(10, 3, 8, 8))    # Tensor - (batch_size, in_channels=4, height=8, width=8)\n",
    "# Set up a quanvolutional layer\n",
    "test_quanv_layer = Quanvolution(out_channels=4, kernel_size=(2, 2), stride=(2,2))\n",
    "# Apply the quanvolutional layer to the input tensor\n",
    "test_output_tensor = test_quanv_layer.forward(test_input_tensor)    # Tensor - (batch_size, out_channels=4, height=4, width=4)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "1b2c7854",
   "metadata": {},
   "source": [
    "The size of the convolutional (or quanvolutional) layer output $o_j$ along axis $j$ depends on the following paremeters:\n",
    "\n",
    "* $i_j$: input size along axis j;\n",
    "* $k_j$: kernel size along axis j;\n",
    "* $s_j$: stride (distance between two consecutive positions of the kernel) along axis j;\n",
    "* $p_j$: zero padding (number of zeros concatenated at the beginning and at the end of an axis) along axis j.\n",
    "\n",
    "$$o_j = \\left \\lfloor \\frac{i_j + 2p_j - k_j}{s_j} \\right \\rfloor$$\n",
    "\n",
    "Performing convolution (or quanvolution) with kernel of size (2, 2) and stride (2, 2) halves the size of the input. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "id": "608bdac5",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "torch.Size([10, 4, 4, 4])\n"
     ]
    }
   ],
   "source": [
    "# Check the size of the output feature map\n",
    "print(test_output_tensor.size())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "37e8f880",
   "metadata": {},
   "source": [
    "## Integrating a Quanvolutional Layer into a Classical Neural Network"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "id": "043499b7",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Usage example: building a hybrid regressor model.\n",
    "\n",
    "class regressor_quanv(nn.Module):\n",
    "    def __init__(self):\n",
    "        super(regressor_quanv, self).__init__()\n",
    "\n",
    "        # Tensor - (N_batch, Channels = 1, Height = 50, Width = 22)\n",
    "        self.conv0 = nn.Conv2d(1, 22, kernel_size=(1, 22), stride=(1, 1))\n",
    "        self.conv0_bn = nn.BatchNorm2d(22)\n",
    "\n",
    "        # Tensor - (N_batch, Channels = 22, Height = 50, Width = 1)\n",
    "        self.conv1 = nn.Conv2d(22, 250, kernel_size=(2, 1), stride=(2, 1))\n",
    "        self.conv1_bn = nn.BatchNorm2d(250)\n",
    "\n",
    "        # Tensor - (N_batch, Channels = 250, Height = 25, Width = 1)\n",
    "        self.conv2 = nn.Conv2d(250, 350, kernel_size=(5, 1), stride=(5, 1))\n",
    "        self.conv2_bn = nn.BatchNorm2d(350)\n",
    "\n",
    "        # Tensor - (N_batch, Channels = 350, Height = 5, Width = 1)\n",
    "        self.quanv = Quanvolution(450, kernel_size=(5, 1), stride=(1, 1))\n",
    "        self.quanv_bn = nn.BatchNorm2d(450)\n",
    "\n",
    "        # Tensor - (N_batch, Channels = 450, Height = 1, Width = 1)\n",
    "        self.efc01 = nn.Linear(450, 1)\n",
    "        self.efc0 = nn.Linear(450, 45)\n",
    "        self.efc0_bn = nn.BatchNorm1d(45)\n",
    "        self.efc1 = nn.Linear(45, 1)\n",
    "\n",
    "    def forward(self, x):\n",
    "        h0 = F.relu(self.conv0_bn(self.conv0(x.view(-1, 1, 50, 22))))\n",
    "        h1 = F.relu(self.conv1_bn(self.conv1(h0)))\n",
    "        h2 = F.relu(self.conv2_bn(self.conv2(h1)))\n",
    "        h3 = F.relu(self.quanv_bn(self.quanv(h2)))\n",
    "        h4 = F.relu(self.efc0_bn(self.efc0(h3.view(-1, 450))))\n",
    "\n",
    "        return self.efc1(h4) + self.efc01(h3.view(-1, 450))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e7ce10ae",
   "metadata": {},
   "source": [
    "## Resources"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "890ecc26",
   "metadata": {},
   "source": [
    "1. Goodfellow I., Bengio Y., Courville A. Deep Learning. MIT Press (2016).\n",
    "   https://www.deeplearningbook.org/contents/convnets.html\n",
    "2. Dumoulin V., Visin F. A guide to convolution arithmetic for deep learning (2016). \n",
    "   https://arxiv.org/abs/1603.07285\n",
    "3. Henderson, M., Shakya, S., Pradhan, S. et al. Quanvolutional neural networks: powering image recognition with quantum circuits. Quantum Mach. Intell. 2, 2 (2020).\n",
    "   https://arxiv.org/abs/1904.04767\n",
    "4. Mattern D., Martyniuk D., Willems H., Bergmann F., Paschke A. Variational Quanvolutional Neural Networks with enhanced image encoding (2021). \n",
    "   https://arxiv.org/pdf/2106.07327.pdf\n",
    "5. Le, P.Q., Dong, F. & Hirota, K. A flexible representation of quantum images for polynomial preparation, image compression, and processing operations. Quantum Inf Process 10, 63–84 (2011)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "chem-qml-env",
   "language": "python",
   "name": "chem-qml-env"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
