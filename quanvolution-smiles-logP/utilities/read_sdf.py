from rdkit import Chem


fh = open('../data/logP.smi', 'w')
suppl = Chem.SDMolSupplier('../data/LogP_QR.sdf')
for mol in suppl:
    if mol is not None:
        cs = mol.GetProp('Canonical_QSARr')
        lp = mol.GetProp('LogP')
        fh.write(cs + ',' + lp + '\n')
fh.close()
