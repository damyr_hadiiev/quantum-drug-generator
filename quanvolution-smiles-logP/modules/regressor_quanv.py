import torch
from torch import nn
from torch.nn import functional as F
from modules.quanvolution import *


class regressor_quanv(nn.Module):
    def __init__(self):
        super(regressor_quanv, self).__init__()

        # Tensor - (N_batch, Channels = 1, Height = 50, Width = 22)
        self.conv0 = nn.Conv2d(1, 22, kernel_size=(1, 22), stride=(1, 1))
        self.conv0_bn = nn.BatchNorm2d(22)

        # Tensor - (N_batch, Channels = 22, Height = 50, Width = 1)
        self.conv1 = nn.Conv2d(22, 250, kernel_size=(2, 1), stride=(2, 1))
        self.conv1_bn = nn.BatchNorm2d(250)

        # Tensor - (N_batch, Channels = 250, Height = 25, Width = 1)
        self.conv2 = nn.Conv2d(250, 350, kernel_size=(5, 1), stride=(5, 1))
        self.conv2_bn = nn.BatchNorm2d(350)

        # Tensor - (N_batch, Channels = 350, Height = 5, Width = 1)
        self.quanv = Quanvolution(450, kernel_size=(5, 1), stride=(1, 1))
        self.quanv_bn = nn.BatchNorm2d(450)

        # Tensor - (N_batch, Channels = 450, Height = 1, Width = 1)
        self.efc01 = nn.Linear(450, 1)
        self.efc0 = nn.Linear(450, 45)
        self.efc0_bn = nn.BatchNorm1d(45)
        self.efc1 = nn.Linear(45, 1)

    def forward(self, x):
        h0 = F.relu(self.conv0_bn(self.conv0(x.view(-1, 1, 50, 22))))
        #print("Conv 0")
        h1 = F.relu(self.conv1_bn(self.conv1(h0)))
        #print("Conv 1")
        h2 = F.relu(self.conv2_bn(self.conv2(h1)))
        #print("Conv 2")
        h3 = F.relu(self.quanv_bn(self.quanv(h2)))
        #print("Quanv")
        h4 = F.relu(self.efc0_bn(self.efc0(h3.view(-1, 450))))
        #print('Finish')

        return self.efc1(h4) + self.efc01(h3.view(-1, 450))