import torch
from torch.nn import functional as F


def loss_function(a, b):
    return F.mse_loss(a, b, reduction='sum')


def train(epoch, model, optimizer, log_interval, train_loader):
    model.train()
    train_loss = 0
    for batch_idx, (y, x) in enumerate(train_loader):
        optimizer.zero_grad()
        recon_y = model(x)
        loss = loss_function(recon_y, y)
        loss.backward()
        train_loss += loss.item()
        optimizer.step()
        if batch_idx % log_interval == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(x), len(train_loader.dataset),
                100. * batch_idx / len(train_loader),
                loss.item() / len(x)))
    train_loss /= len(train_loader.dataset)
    print('====> Epoch: {} Average loss on train set: {:.6f}'.format(epoch, train_loss))
    return train_loss


def test(epoch, model, test_loader):
    model.eval()

    test_loss = 0
    with torch.no_grad():
        for i, (y, x) in enumerate(test_loader):
            recon_y = model(x)
            test_loss += loss_function(recon_y, y).item()
    test_loss /= len(test_loader.dataset)
    print('====> Epoch: {} Average loss on test set:  {:.6f}\n'.format(epoch, test_loss))
    return test_loss
