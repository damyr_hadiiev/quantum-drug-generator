import torch
from torch import nn
import pennylane as qml
import numpy as np
from numpy.random import default_rng
from pennylane.templates import AngleEmbedding, RandomLayers
import time


def quanvolution_circuit(q_input, rand_params, seed=42):
    """A quantum circuit function corresponding to the quanvolutional kernel."""
    n_qubits = len(q_input)

    # Encode (n_qubits) classical input values into a quantum state by the action of RY gates
    qml.AngleEmbedding(np.pi * q_input, wires=range(n_qubits), rotation='Y')

    # Apply a random unitary transformation (a combination of parametrized rotational gates RX, RY, RZ
    # and entangling gates CX)
    qml.RandomLayers(rand_params, wires=list(range(n_qubits)), seed=seed)

    # Measure each qubit in the computational basis to produce (n_qubits) classical output values
    meas_res = [qml.expval(qml.PauliZ(qubit)) for qubit in range(n_qubits)]
    return tuple(meas_res)


class Quanvolution(nn.Module):
    """A quantum extension of a convolutional layer with classical filters replaced by parametrized
       quantum circuits. Consists of (out_channels) filters that locally transform input data in a
       sliding window manner to produce feature maps.
        Args:
            out_channels (int): Number of channels produced by the convolution (number of filters)
            kernel_size (tuple): Size of the convolving kernel.
            stride (tuple): Stride of the convolution (distance between two consecutive
                            positions of the kernel over the input tensor).
            padding (tuple): Amount of zero-padding added to the sides of the input. Default: (0,0)
            n_quanv_layers (int): Number of random layers in the quantum circuit corresponding to the
                                  convolving kernel. Default: 1
        """
    def __init__(self, out_channels=None, kernel_size=None, stride=None, padding=(0, 0), n_quanv_layers=1):
        super(Quanvolution, self).__init__()
        self.out_channels = out_channels
        self.kernel_size = kernel_size
        self.stride = stride
        self.padding = padding
        self.n_quanv_layers = n_quanv_layers
        self.n_qubits = int(np.prod(np.array(self.kernel_size)))
        self.dev = qml.device('default.qubit', wires=self.n_qubits)
        self.circuit = qml.QNode(quanvolution_circuit, self.dev, interface="torch")

    def __str__(self):
        return "Quanvolution(out_channels=%s, kernel_size=%s, stride=%s, padding=%s, n_quanv_layers=%s)" \
               % (self.out_channels, self.kernel_size, self.stride, self.padding, self.n_quanv_layers)

    def __repr__(self):
        return "Quanvolution(out_channels=%s, kernel_size=%s, stride=%s, padding=%s, n_quanv_layers=%s)" \
               % (self.out_channels, self.kernel_size, self.stride, self.padding, self.n_quanv_layers)

    def forward(self, input_data):
        batch_size = input_data.size(dim=0)
        in_channels = input_data.size(dim=1)
        height = input_data.size(dim=2)
        width = input_data.size(dim=3)

        # Determine the size of the output tensor
        out_size = ((height + 2 * self.padding[0] - self.kernel_size[0]) // self.stride[0] + 1,
                    (width + 2 * self.padding[1] - self.kernel_size[1]) // self.stride[1] + 1)

        # Apply zero-padding of the given size to the input data
        vertical_padding = torch.zeros(batch_size, in_channels, self.padding[0], width)
        horizontal_padding = torch.zeros(batch_size, in_channels, height + 2 * self.padding[0], self.padding[1])
        input_data = torch.cat((vertical_padding, input_data, vertical_padding), dim=2)
        input_data = torch.cat((horizontal_padding, input_data, horizontal_padding), dim=3)

        # Set up and seed random number generators
        torch.manual_seed(0)
        rng = default_rng(0)

        # Create an empty tensor to store output feature maps
        output_features = torch.zeros(batch_size, self.out_channels, in_channels, out_size[0], out_size[1])

        # Quanvolution
        for f in range(self.out_channels):
            t_filter_start = time.time()
            for c in range(in_channels):
                t_channel_start = time.time()
                # Generate random parameters for a random circuit representing a quanvolutional kernel
                rand_params = torch.nn.Parameter(2 * np.pi * torch.rand(self.n_quanv_layers, self.n_qubits),
                                                 requires_grad=False)
                # Generate a random seed for the random circuit
                circuit_seed = rng.integers(1000, size=1)

                for smiles in range(batch_size):
                    t_smiles_start = time.time()
                    print('Filter: ' + str(f+1) + '  ' + 'Channel: ' + str(c+1) + '  ' + 'SMILE:' + str(smiles+1))
                    conv_row = -1

                    for i in range(0, height, self.stride[0]):
                        if i + self.kernel_size[0] > height:
                            break
                        else:
                            conv_column = -1
                            conv_row += 1

                            for j in range(0, width, self.stride[1]):
                                if j + self.kernel_size[1] > width:
                                    break
                                else:
                                    conv_column += 1
                                    # Select the input subsection to be transformed
                                    selected_input_area = input_data[smiles, c, i:i + self.kernel_size[0],
                                                          j:j + self.kernel_size[1]]
                                    q_in = torch.flatten(selected_input_area)
                                    t_circ_start = time.time()
                                    # Execute quantum circuit
                                    q_out = self.circuit(q_in, rand_params, seed=circuit_seed)
                                    t_circ_end = time.time()
                                    # Postprocessing of quantum measurement results
                                    average_out = (torch.sum(q_out) / self.n_qubits).item()
                                    output_features[smiles][f][c][conv_row][conv_column] = average_out
                                    # print("Circuit execution time: ", t_circ_end-t_circ_start)
                    t_smiles_end = time.time()
                    # print("Convolution time for a single SMILES: ", t_smiles_end - t_smiles_start)
                t_channel_end = time.time()
                # print("Convolution time for a single channel: ", t_channel_end - t_channel_start)
            t_filter_end = time.time()
            # print("Convolution time for a single filter: ", t_filter_end - t_filter_start)

        # Summation over the input channels dimension
        output_features = torch.sum(output_features, dim=2)

        return output_features
