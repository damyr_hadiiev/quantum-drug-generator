# Quanvolutional Neural Network (QNN)

## Overview

Henderson et al (2020) introduced the concept of a quantum convolutional neural network (quanvolutional neural network or QNN) as an extension of the classical CNN aiming to leverage the advantages of quantum computing techniques while taking into account the limitations of the available quantum hardware. Architecturally, QNNs are very similar to their classical counterparts. However, a new type of transformational layer – a quanvolutional layer – is introduced. It consists of a set of filters (kernels) each represented by a parametrized quantum circuit. Even though a quanvolutional layer differs from its classical predecessor in the nature of transformations to the data, the fundamental idea behind it remains. Namely, the input data is locally processed by a sequence of filters to produce abstract feature maps. Thus, a quanvolutional layer can be viewed as a quantum subroutine in an otherwise classical machine learning model. This means that conceptually the QNN framework follows a hybrid quantum-classical approach and could potentially bring some degree of improvement compared to the classical models when implemented on NISQ era quantum devices. It was experimentally shown that in comparison to classical CNNs it exhibits higher test set accuracy as well as shorter training time when applied to the image classification problem.

QNN framework also has a range of advantages compared to many other QML models. Firstly, due to the local character of the quanvolutional transformations the requirement on the number of qubits is relatively low. Moreover, parametrized circuits playing the role of quanvolutional filters are relatively shallow. Another powerful benefit of the quanvolutional algorithm is its potential resiliency to the consistent noise models.

## Important Files

A detailed description of the quanvolutional layer implementation with usage examples is provided in the notebook *QNN.ipynb*. Please refer to this file in order to find helpful instructions on how to create a quantum circuit representing a quanvolutional filter, initialize a quanvolutional layer and integrate it into a neural network stack to construct a QNN.

https://gitlab.com/damyr_hadiiev/quantum-drug-generator/-/blob/quanvolution-smiles-logP/quanvolution-smiles-logP/QNN.ipynb

The *modules* folder cointains the following files crucial to build and run a QNN:
* quanvolution.py: Contains a quantum circuit function *quanvolution_circuit* corresponding to a quanvolutional filter and *Quanvolution* class, which holds the implementation of a quanvolutional layer (the number of filters in the layer, the kernel size, the stride and the padding are up to user's specification).

https://gitlab.com/damyr_hadiiev/quantum-drug-generator/-/blob/quanvolution-smiles-logP/quanvolution-smiles-logP/modules/quanvolution.py
* regressor_quanv.py: Contains *regressor_quanv* class used to construct a QNN model, which, apart from the quanvolutional layer, includes classical convolutional layers as well fully connected layers. Since our implementation of a quanvolutional layer is highly flexible, this file can be modified by rearranging (or adding/removing) layers in the stack to adjust the model to a specific problem.

https://gitlab.com/damyr_hadiiev/quantum-drug-generator/-/blob/quanvolution-smiles-logP/quanvolution-smiles-logP/modules/regressor_quanv.py

## References

Henderson, M., Shakya, S., Pradhan, S. et al. Quanvolutional neural networks: powering image recognition with quantum circuits. Quantum Mach. Intell. 2, 2 (2020).

https://doi.org/10.1007/s42484-020-00012-y

https://arxiv.org/abs/1904.04767
