# ReLeaSE Algorithm Workflow + Quantum Generator using QGRU

## Introduction
The original ReLeaSE algorithm utilized a stack-augmented RNN technique that can use either Long Short Term Memory (LSTM) or GRU for their generative model. In an earlier project that won an award in QHACK, SoftServe had showcased a version of LSTM called QLSTM using quantum variational layers to improve on the original LSTM. In this particular project, we show that we can use similar principles to improve GRU to get QGRU.

GRU (including QGRU) and LSTM are RNN techniques, which are widely used for many tasks that involves sequential data. It works by utilizing some of the neural network layers to form a loop, attaching more importance the sequence of items rather than the individual items themselves. They are used quite widely for time series data, audio data, and in our case, a type of Natural Language Processing data. Being a representation of molecules, the sequence of the individual elements are as important if not more important than just what elements are inside the molecule, making the RNN techniques suitable for our generator.

## Important files

To look at how the entire ReLeaSE Algorithm Workflow will work with the Quantum GRU, please look at the notebook "Reinforcement Learning + Generator". It offers a detailed explanation of how to run the entire code.

Beyond that, we see that there are many folders of modules that the entire process uses. A brief description of the important files in those folders will be found here:

release folder:

This folder has all the modules to run the generator and the RL algorithm
- quantum.py: Contains the class QGRU, which is the main quantum algorithm used in this section.
- data.py: Contains the code and classes necessary to process the data files
- stackRNN.py: Contains the stacked RNNs class, which is the class used for the generator be it classical or quantum
- reinforcement.py: Has the class that runs the policy gradient reinforcement learning

data folder:

This folder contains all the data files.
- logP_red_3.smi: Main data file that we use. It is a reduced dataset to make training more efficient.
- smiles.train.csv and smiles.test.csv. It has the SMILES with the correct elements tailored for the predictor

checkpoint folder:

This folder contains the parameters for the generator models we train.

models folder:

This folder contains the parameters for the predictor models we import.

modules folder:

This folder contains the necessary files for the predictor models we import.

## ReLeaSE method paper that we based our algorithm on:
Mariya Popova, Olexandr Isayev, Alexander Tropsha. *Deep Reinforcement Learning for de-novo Drug Design*. Science Advances, 2018, Vol. 4, no. 7, eaap7885. DOI: [10.1126/sciadv.aap7885](http://dx.doi.org/10.1126/sciadv.aap7885)
