import sys
from rdkit import Chem
from rdkit.Chem import Descriptors

if len(sys.argv) > 1:
    smiles_string = sys.argv[1]
    mol = Chem.MolFromSmiles(smiles_string)
    if mol is not None:
        molwt = Descriptors.ExactMolWt(mol)
        if 180 <= molwt < 450:
            print('Mass is good')
        else:
            print('Mass is not good')
    else:
        print('incorrect SMILES string')
else:
    print('SMILES string is expected as argument')
