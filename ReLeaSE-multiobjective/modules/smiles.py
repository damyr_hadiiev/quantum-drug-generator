import torch
import torch.utils.data
import re
from rdkit import Chem
from rdkit import rdBase


class SMILESDataset(torch.utils.data.Dataset):
    def __init__(self, path_to_csv_file, size_of_smiles_dict):
        fh = open(path_to_csv_file, 'r')

        self.labels = []
        self.onehots = []

        for line in fh:
            label = torch.zeros(1)
            label[0], onehot_r, onehot_l = csv_to_onehot(line, size_of_smiles_dict)
            self.labels.append(label)
            self.onehots.append(onehot_r)
            self.labels.append(label)
            self.onehots.append(onehot_l)
        fh.close()

        self.length = len(self.labels)
        self.number_of_columns = self.onehots[0].shape[0]

    def __len__(self):
        return self.length

    def __getitem__(self, index):
        return self.labels[index], self.onehots[index]

    def col_num(self):
        return self.number_of_columns


def make_smiles_dict(path_to_dictionary):
    smiles_dict = {}
    fh = open(path_to_dictionary, 'r')
    for i, line in enumerate(fh):
        smiles_dict[i] = line.strip()
    fh.close()
    return smiles_dict


def csv_to_onehot(csv_line, size_of_smiles_dict):
    arr_r = csv_line.strip().split(',')
    label = float(arr_r[-1])
    del arr_r[0]
    del arr_r[-1]

    arr_l = arr_r.copy()
    while '0' in arr_l: arr_l.remove('0')
    arr_l = ['0'] * (len(arr_r) - len(arr_l)) + arr_l

    onehot_r = torch.zeros(len(arr_r), size_of_smiles_dict)
    onehot_l = torch.zeros(len(arr_l), size_of_smiles_dict)
    for i in range(len(arr_r)):
        onehot_r[i, int(arr_r[i])] = 1
        onehot_l[i, int(arr_l[i])] = 1
    return label, onehot_r, onehot_l
