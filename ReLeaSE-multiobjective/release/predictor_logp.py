import sys
import pickle
import torch
import numpy as np
from tqdm import tqdm
from rdkit import Chem
import re, random
sys.path.append('./chemfiles/')

from openchem.models.Smiles2Label import Smiles2Label
from openchem.modules.embeddings.basic_embedding import Embedding
from openchem.modules.encoders.rnn_encoder import RNNEncoder
from openchem.modules.mlp.openchem_mlp import OpenChemMLP
from openchem.utils.utils import identity
from openchem.data.utils import pad_sequences, seq2tensor, sanitize_smiles

#smiles_dict = make_smiles_dict(path_to_dictionary)
size_of_smiles_dict = 22

def mol_to_elements(smi, mol):
    molecule = []
    for a in mol.GetAtoms():
        atom = a.GetSymbol()
        if atom not in molecule:
            molecule.append(atom)
    molecule = sorted(molecule, key=len, reverse=True)
    return list(filter(None, re.split(r'('+'|'.join(molecule)+'|\d|\(|\)|\#|\[|\]|\=|\.|\-|\+)', smi)))

def mol_to_smi_c(mol):
    return Chem.MolToSmiles(mol, kekuleSmiles=True)

elements = ["0", "1", "2", "3", "4", "#", "=", "[", "]", 
         "(", ")", "H", "C", "N", "O", "P", "S", "Na", "F", "Cl", "Br", "I"]

smiles_length = 50

def get_mol(smi):
    
    mol = Chem.MolFromSmiles(smi)
    if mol is not None:
        smi_c = mol_to_smi_c(mol)
        try:
            molecule = mol_to_elements(smi_c, mol)
            if len(molecule) <= smiles_length and all(element in elements for element in molecule):
                sample = smi_c
                for j in range(smiles_length):
                    if j < len(molecule):
                        sample += ',' + str(elements.index(molecule[j]))
                    else:
                        sample += ',0'
        except:
            pass       
          
    return sample

def to_onehot(sam):
    arr = get_mol(sam).strip().split(',')
    del arr[0]
    
    onehot = torch.zeros(len(arr), size_of_smiles_dict)
    for i in range(len(arr)):
        onehot[i, int(arr[i])] = 1
    return onehot

    
class Predictor(object):
    def __init__(self, model, tokens):
        super(Predictor, self).__init__()
        
        self.model = model
        self.tokens = tokens

    def predict(self, smiles, use_tqdm=False):
        double = False
        canonical_smiles = []
        invalid_smiles = []
        if use_tqdm:
            pbar = tqdm(range(len(smiles)))
        else:
            pbar = range(len(smiles))
        for i in pbar:
            sm = smiles[i]
            if use_tqdm:
                pbar.set_description("Calculating predictions...")
            try:
                sm_mol = Chem.MolFromSmiles(sm)
                sm = Chem.MolToSmiles(sm_mol, kekuleSmiles=True)
                molecule = mol_to_elements(sm, sm_mol)
                if len(molecule) <= smiles_length and all(element in elements for element in molecule) and len(sm) != 0:
                    canonical_smiles.append(sm)
                else:
                    invalid_smiles.append(sm)
                
            except:
                invalid_smiles.append(sm)
        if len(canonical_smiles) == 0:
            return canonical_smiles, [], invalid_smiles
        if len(canonical_smiles) == 1:
            double = True
            canonical_smiles = [canonical_smiles[0], canonical_smiles[0]]
        padded_smiles, length = pad_sequences(canonical_smiles)
        smiles_tensor, _ = seq2tensor(padded_smiles, self.tokens, flip=False)
        prediction = []
        for i in canonical_smiles:
            sam = to_onehot(i)
            pred = self.model(sam)
            prediction.append(pred)
        
        #for i in range(len(self.model)):
        #    prediction.append(
        #       self.model[i]([torch.LongTensor(smiles_tensor).cuda(),
        #                       torch.LongTensor(length).cuda()],
        #                      eval=True).detach().cpu().numpy())
        #prediction = np.array(prediction).reshape(len(self.model), -1)
        #prediction = np.min(prediction, axis=0)
        if double:
            canonical_smiles = canonical_smiles[0]
            prediction = [prediction[0]]
        return canonical_smiles, prediction, invalid_smiles