import pennylane as qml

from pennylane import numpy as np
from pennylane.templates import RandomLayers

import tensorflow as tf
from tensorflow import keras

import time

from itertools import product
from itertools import combinations_with_replacement


# Parameters

# qml_devices = ["lightning.gpu", "default.qubit"]
# qml_devices = ["lightning.gpu"]

qml_devices = ["default.qubit"]

image_width = 28
image_length = 28

n_layers = 1

factors = [1, 2, 4, 7, 14, 28]

np.random.seed(0)
tf.random.set_seed(0)


# Load MNIST dataset

mnist_dataset = keras.datasets.mnist
train_data, test_data = mnist_dataset.load_data()

train_images, train_labels = train_data
test_images, test_labels = test_data

# Normalize pixels

train_images = train_images / 255
test_images = test_images / 255

# Add extra dimension for convolution channels

train_images = np.array(train_images[..., tf.newaxis], requires_grad=False)
test_images = np.array(test_images[..., tf.newaxis], requires_grad=False)

image = train_images[0]


### Quanvolution

def kernel_circuit(pixel_values):    

    for qubit, pixel_value in enumerate(pixel_values):      

        theta = np.pi * pixel_value        
        qml.RY(theta, wires=qubit)        

    RandomLayers(random_layer_parameters, wires=qubits)

    measurement_result = [qml.expval(qml.PauliZ(qubit)) for qubit in qubits]

    return measurement_result


def quanvolve(image):
    """Convolve input image with many applications of the same quantum kernel circuit."""

    # Initialize

    features = np.zeros((feature_width, feature_length, n_channels))

    deltas = list(product(range(kernel_width),
                          range(kernel_length)))

    # Loop over image

    for corner_x in range(0, image_width, kernel_width):
        for corner_y in range(0, image_length, kernel_length):            

            pixel_values = []

            for x_delta, y_delta in deltas:

                x = corner_x + x_delta
                y = corner_y + y_delta

                pixel_values.append(np.squeeze(image[x, y]))

            # Call kernel

            kernel_outputs = bound_kernel_circuit(pixel_values)

            # Feature outputs

            feature_corner_x = corner_x // kernel_width
            feature_corner_y = corner_y // kernel_length

            for channel, kernel_output in enumerate(kernel_outputs):

                features[feature_corner_x,
                         feature_corner_y,
                         channel] = kernel_output            

    return features



### Benchmark


# Prepare experiments

kernel_sizes = combinations_with_replacement(factors, 2)

experiment_list = []

for kernel_size, qml_device in product(kernel_sizes, qml_devices):

    kernel_width, kernel_length = kernel_size
        
    kernel_surface = kernel_width * kernel_length

    experiment = {"qml_device": qml_device,
                  "kernel_width": kernel_width, 
                  "kernel_length": kernel_length,
                  "kernel_surface": kernel_surface}

    experiment_list.append(experiment)

sorted_experiments = sorted(experiment_list, 
                            key=lambda experiment: experiment['kernel_surface'])

experiments = dict(enumerate(sorted_experiments))


# Printout

print("Planned experiments:")

for experiment_id, experiment in experiments.items():    
    
    kernel_width = experiment['kernel_width']
    kernel_length = experiment['kernel_length']
    kernel_surface = experiment['kernel_surface']
    
    print(f"{experiment_id} - Kernel: {kernel_width} X {kernel_length} = {kernel_surface}")


# Run experiments

for experiment_id, experiment in experiments.items():
    
    start_time = time.time()
    
    kernel_width = experiment['kernel_width']
    kernel_length = experiment['kernel_length']
    kernel_surface = experiment['kernel_surface']
    
    n_channels = n_qubits = kernel_width * kernel_length

    qubits = list(range(n_qubits))

    feature_width = image_width // kernel_width
    feature_length = image_length // kernel_length
    
    
    # QML device
    
    qml_device_instance = qml.device(qml_device, wires=n_qubits) 
    
    bound_kernel_circuit = qml.qnode(qml_device_instance)(kernel_circuit)

    random_layer_parameters = np.random.uniform(high=2*np.pi,
                                                size=(n_layers, n_channels)) 

    # Printout
    
    print(f"")
    print(f"===================")
    print(f"Experiment: {experiment_id}")
    print(f"Kernel: {kernel_width} X {kernel_length} = {kernel_surface}")
    print(f"Device: {qml_device}")
    
    print("\nQuantum pre-processing of image...") 
    
    quanvolve(image)
    
    # Time
    
    execution_time = time.time() - start_time
    
    experiments[experiment_id]['execution_time'] = execution_time

    print(f"\nExecution time: {execution_time:.2f} seconds")