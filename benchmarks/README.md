# Benchmarks

Below benchmarks have been developed to evaluate quantum algorithm execution times:

- [Quanvolution](#quanvolution)
- [Differentiation methods](#differentiation-methods)
- [Quantum Error Mitigation](#quantum-error-mitigation)
- [cuQuantum](#cuquantum)
- [JAX](#jax)
- [Cirq qsim](#cirq-qsim)


## Quanvolution

Quanvolutional Neural Network (QNN) layer with arbitrary configuration:

- Parameters:

    pennylane backend\
    image size\
    stride size\
    filter size\
    filters count\
    trainanble quantum layers count

[quanvolution/quanvolution_benchmark.ipynb](quanvolution/quanvolution_benchmark.ipynb)


## Differentiation methods

Benchmark for different combinations of:

- Differentiation methods:

    backprop\
    adjoint

    parameter-shift\
    finite-diff

- Pennylane backends:

    default.qubit\
    default.qubit.jax

    lightning.qubit\
    lightning.gpu

    cirq.simulator\
    cirq.qsim

[diff_methods/diff_benchmark.ipynb](diff_methods/diff_benchmark.ipynb)


## Quantum Error Mitigation

Zero Noise Extrapolation (ZNE) quantum error mitigation protocol applied on:

- Circuit on "default.mixed" backend:

    [benchmarks/error_mitigation/pennylane_zne.ipynb](benchmarks/error_mitigation/pennylane_zne.ipynb)

- Quanvolutional layer benchmark:

    [benchmarks/error_mitigation/quanvolution_mitigation_benchmark.ipynb](benchmarks/error_mitigation/quanvolution_mitigation_benchmark.ipynb)


## cuQuantum

Benchmark for direct cuQuantum circuits:

- Arbitrary Unitary
- H x H @ CNOT

[cuquantum/cuquantum_benchmark.ipynb](cuquantum/cuquantum_benchmark.ipynb)


## JAX

Benchmarks for Google JAX numerical transformation framework:

Features:

- Vectorization (vmap)
- Parallelization (pmap)
- Compilation (jit)

Applied for Quanvolution layer:

[benchmarks/jax/jax_quanvolution.ipynb](benchmarks/jax/jax_quanvolution.ipynb)


## Cirq qsim

Benchmark for Google Cirq qsim simulator with multi-threading CPU, GPU and multi-GPU support:

[benchmarks/other/cirq_multi_gpu.ipynb](benchmarks/other/cirq_multi_gpu.ipynb)
